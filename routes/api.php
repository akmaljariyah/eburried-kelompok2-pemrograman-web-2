<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'API'], function () {

    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');

    Route::resource('/burial', 'BurialController', ['only' => ['index', 'store']]);
    Route::resource('/blok', 'BlokController', ['only' => ['index', 'store']]);
    Route::resource('/unit', 'UnitController', ['only' => ['index', 'store']]);
    Route::resource('/corpse', 'CorpseController', ['only' => ['index', 'store', 'show']]);
    Route::resource('/family', 'FamilyController', ['only' => ['index', 'store']]);
    Route::resource('/invoice', 'InvoiceController', ['only' => ['index', 'store']]);
    Route::resource('/payment', 'PaymentController', ['only' => ['store']]);
    
    Route::get('/count-data', 'GeneralController@getCountData');
    Route::get('/unit/{unitId}/invoice', 'InvoiceController@getUnitInvoices');

});

