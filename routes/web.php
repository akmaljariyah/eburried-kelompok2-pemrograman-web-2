<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/blok', function () {
    return view('blok');
});

Route::get('/unit', function () {
    return view('unit');
});

Route::get('/guest', function () {
    return view('guest');
});

Route::get('/invoice', function () {
    return view('invoice');
});
