<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blok;
use App\Models\Unit;
use Exception;
use Illuminate\Support\Facades\Log;

class UnitController extends Controller
{
    public function index() {
        try
        {
            $units = Unit::get();

            $data = [];
            
            foreach($units as $unit) {

                $data[] = [
                    'id' => $unit->id,
                    'blok' => $unit->blok,
                    'cost_per_month' => $unit->cost_per_month,
                    'name' => $unit->name,
                    'status' => $unit->status,
                    'corpse' => $unit->corpse,               
                ];
            }
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $units
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function store(Request $request) {
        try
        {
            $blok = Blok::where('id', $request->blok_id)->first();
            $code = $blok ? 1 : 0;
            $message = $blok ? "Insert data success" : "Insert data failed, BlokId not found";

            $unit = $code == 1 ? Unit::create([
                'blok_id' => $request->blok_id,
                'cost_per_month' => $request->cost_per_month,
                'name' => $request->name,
                'status' => 1
            ]) : [];
    
            return response()->json([
                'code' => $code,
                'message' => $message,
                'data' => $unit
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }
}
