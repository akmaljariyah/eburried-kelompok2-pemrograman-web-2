<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Burial;
use App\Models\Blok;
use App\Models\Unit;
use App\Models\Corpse;
use App\Models\Invoice;
use Exception;
use Illuminate\Support\Facades\Log;

class GeneralController extends Controller
{
    public function getCountData() {
        try
        {
            $burials = Burial::get();
            $bloks = Blok::get();
            $units = Unit::get();
            $corpses = Corpse::get();
            $invoices = Invoice::get();
            $amountInvoice = 0;
            $data = [];
            foreach ($invoices as $invoice) {
                $data[] = $invoice;
                if($invoice->payment != null) {
                    $amountInvoice += (float) $invoice->amount;
                }
            }

            $data = [
                'burial' => count($burials),
                'blok' => count($bloks),
                'unit' => count($units),
                'corpse' => count($corpses),
                'iuran' => $amountInvoice
            ];
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $data
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }
}
