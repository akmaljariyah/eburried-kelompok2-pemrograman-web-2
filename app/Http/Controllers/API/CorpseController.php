<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Corpse;
use App\Models\Unit;
use Exception;
use Illuminate\Support\Facades\Log;

class CorpseController extends Controller
{
    public function index() {
        try
        {
            $corpses = Corpse::get();
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $corpses
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function show(Corpse $corpse) {
        try
        {    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $corpse->load('families')
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function store(Request $request) {
        try
        {
            $unit = Unit::where('id', $request->unit_id)->first();
            $unit->status = 2;
            $unit->save();
            
            $code = $unit ? 1 : 0;
            $message = $unit ? "Insert data success" : "Insert data failed, UnitId not found";

            $corpse = $code == 1 ? Corpse::create([
                'unit_id' => $request->unit_id,
                'name' => $request->name,
                'address' => $request->address,
                'die_at' => $request->die_at,
                'burried_at' => $request->burried_at
            ]) : [];
    
            return response()->json([
                'code' => $code,
                'message' => $message,
                'data' => $corpse
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }
}
