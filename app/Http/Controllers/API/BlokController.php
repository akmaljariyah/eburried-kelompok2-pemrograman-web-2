<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Burial;
use App\Models\Blok;
use Exception;
use Illuminate\Support\Facades\Log;

class BlokController extends Controller
{
    public function index() {
        try
        {
            $bloks = Blok::get();
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $bloks
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function store(Request $request) {
        try
        {
            $burial = Burial::where('id', $request->burial_id)->first();
            $code = $burial ? 1 : 0;
            $message = $burial ? "Insert data success" : "Insert data failed, BurialId not found";

            $blok = $code == 1 ? Blok::create([
                'burial_id' => $request->burial_id,
                'name' => $request->name
            ]) : [];
    
            return response()->json([
                'code' => $code,
                'message' => $message,
                'data' => $blok
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }
}
