<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Corpse;
use App\Models\Family;
use Exception;
use Illuminate\Support\Facades\Log;

class FamilyController extends Controller
{
    public function index() {
        try
        {
            $families = Family::get();
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $families
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function store(Request $request) {
        try
        {
            $corpse = Corpse::where('id', $request->corpse_id)->first();
            $code = $corpse ? 1 : 0;
            $message = $corpse ? "Insert data success" : "Insert data failed, UnitId not found";

            $family = $code == 1 ? Family::create([
                'corpse_id' => $request->corpse_id,
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'email' => $request->email,
                'status' => $request->status
            ]) : [];
    
            return response()->json([
                'code' => $code,
                'message' => $message,
                'data' => $family
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }
}
