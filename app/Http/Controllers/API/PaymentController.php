<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Payment;
use Exception;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public function store(Request $request) {
        try
        {
            $invoice = Invoice::where('id', $request->invoice_id)->first();
            $code = $invoice ? 1 : 0;
            $message = $invoice ? "Insert data success" : "Insert data failed, UnitId not found";

            $payment = $code == 1 ? Payment::create([
                'invoice_id' => $request->invoice_id,
                'status' => 1,
                'payment_at' => date('Y-m-d h:i:s')
            ]) : [];
    
            return response()->json([
                'code' => $code,
                'message' => $message,
                'data' => $payment
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }
}
