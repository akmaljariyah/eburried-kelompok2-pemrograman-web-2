<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Burial;
use Exception;
use Illuminate\Support\Facades\Log;


class BurialController extends Controller
{
    public function index() {
        try
        {
            $burials = Burial::get();
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $burials
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function store(Request $request) {
        try
        {
            $burial = Burial::create([
                'name' => $request->name,
                'address' => $request->address
            ]);
    
            return response()->json([
                'code' => 1,
                'message' => 'Insert data success',
                'data' => $burial
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }
}
