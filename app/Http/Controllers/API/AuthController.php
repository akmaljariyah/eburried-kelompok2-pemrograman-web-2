<?php

namespace App\Http\Controllers\API;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Hashing\HashManager;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TokenManager;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Models\User;


class AuthController extends Controller
{
    private $hash;
    private $tokenManager;


    public function __construct(Hash $hash, TokenManager $tokenManager)
    {
        $this->hash = $hash;
        $this->tokenManager = $tokenManager;
    }

    public function register(Request $request) {

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $this->hash->make($request->password),
        ]);

        return response()->json($user);

    }

    public function login(Request $request)
    {
        /** @var User $user */
        $user = User::where('email', $request->email)->first();
        
        if (!$user || !$this->hash->check($request->password, $user->password))
        {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }

        $profile = [
            'id'            => $user->id,
            'name'          => $user->name,
            'email'         => $user->email
        ];

        return response()->json([
            'token' => $this->tokenManager->createToken($user)->plainTextToken,
            'profile' => $profile,
        ]);
    }


}
