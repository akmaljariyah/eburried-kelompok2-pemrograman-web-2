<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unit;
use App\Models\Invoice;
use App\Models\Payment;
use Exception;
use Illuminate\Support\Facades\Log;

class InvoiceController extends Controller
{
    public function index() {
        try
        {
            $invoices = Invoice::get();
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $invoices
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }

    public function store(Request $request) {
        try
        {
            $unit = Unit::where('id', $request->unit_id)->first();
            $code = $unit ? 1 : 0;
            $message = $unit ? "Insert data success" : "Insert data failed, UnitId not found";

            $invoice = Invoice::get();
            $number = (count($invoice)+1)."/INV/EB/".date('m')."/".date('Y');

            $invoice = $code == 1 ? Invoice::create([
                'unit_id' => $request->unit_id,
                'period' => date($request->period_year.'-'.$request->period_month.'-01'),
                'number' => $number,
                'amount' => (float) $unit->cost_per_month,
                'due_date' => date($request->period_year.'-'.$request->period_month.'-t')
            ]) : [];
    
            return response()->json([
                'code' => $code,
                'message' => $message,
                'data' => $invoice
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Insert data failed, please call administrator'
            ], 500);
        }
    }

    public function getUnitInvoices($unitId) {
        try
        {
            $invoices = Invoice::where('unit_id', $unitId)->get();
    
            return response()->json([
                'code' => 1,
                'message' => 'Get data success',
                'data' => $invoices->load('payment')
            ]);
        }
        catch(Exception $e) 
        {
            Log::error($e->getMessage());

            return response()->json([
                'code' => 0,
                'message' => 'Get data failed, please call administrator'
            ], 500);
        }
    }
}
