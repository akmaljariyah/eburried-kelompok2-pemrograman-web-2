<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Invoice extends Model
{
    protected $guarded = ['id'];
    
    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    } 
    
    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class);
    } 
}
