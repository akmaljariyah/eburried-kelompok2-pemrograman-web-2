<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Burial extends Model
{
    protected $guarded = ['id'];
    
    public function bloks(): HasMany
    {
        return $this->hasMany(Blok::class);
    } 
}
