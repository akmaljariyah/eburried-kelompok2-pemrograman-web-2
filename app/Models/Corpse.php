<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Corpse extends Model
{
    protected $guarded = ['id'];
    
    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    } 

    public function families(): HasMany
    {
        return $this->hasMany(Family::class);
    } 
}
