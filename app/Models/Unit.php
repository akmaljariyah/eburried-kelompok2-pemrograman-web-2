<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Unit extends Model
{
    protected $guarded = ['id'];
    
    public function blok(): BelongsTo
    {
        return $this->belongsTo(Blok::class);
    } 
    
    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class);
    } 
    
    public function corpse(): HasOne
    {
        return $this->hasOne(Corpse::class);
    } 
}
