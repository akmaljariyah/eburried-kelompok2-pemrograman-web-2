<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Blok extends Model
{
    protected $guarded = ['id'];
    
    public function units(): HasMany
    {
        return $this->hasMany(Unit::class);
    } 
        
    public function blok(): BelongsTo
    {
        return $this->belongsTo(Blok::class);
    } 
}
