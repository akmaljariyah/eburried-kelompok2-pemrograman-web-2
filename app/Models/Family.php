<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Family extends Model
{
    protected $guarded = ['id'];
    
    public function corpse(): BelongsTo
    {
        return $this->belongsTo(Corpse::class);
    } 
}
