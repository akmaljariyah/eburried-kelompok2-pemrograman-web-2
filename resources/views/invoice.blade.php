<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Kelompok 2 - Pemrograman Web</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600;700&display=swap" rel="stylesheet">
    
    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Libraries Stylesheet -->
    <link href="{{ asset('assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css') }}" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container-xxl position-relative bg-white d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->
        <div class="sidebar pe-4 pb-3">
            <nav class="navbar bg-light navbar-light">
                <a href="/" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary"><i class="fa fa-hashtag me-2"></i>eBurrial</h3>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4">
                    <div class="position-relative">
                        <img class="rounded-circle" src="{{ asset('assets/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0">Kelompok 2</h6>
                        <span>Admin</span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="/blok" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Data Blok</a>
                    <a href="/unit" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Data Unit</a>
                    <a href="/invoice" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Data Invoice</a>
                </div>
            </nav>
        </div>
        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
                <a href="/" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><i class="fa fa-hashtag"></i></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>    
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="{{ asset('assets/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex">Kelompok 2</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                            <a href="#" class="dropdown-item">My Profile</a>
                            <a href="#" class="dropdown-item">Settings</a>
                            <a href="#" class="dropdown-item">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->


            <!-- Table Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-sm-12 col-xl-12">
                        <div class="bg-light rounded h-100 p-4">
                            <h6 class="mb-4">Data Iuran</h6>
                            <div style="margin-bottom:20px;">
                                <select id="fSelectUnit" onchange="fetchData()">
                                    <option value="" selected hidden>- Pilih Unit -</option>
                                </select>
                                <br><br>
                                <b>Tambah Tagihan</b>
                                <select id="fSelectMonth">
                                    <option value="">- Pilih Bulan -</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                <select id="fSelectYear">
                                    <option value="">- Pilih Tahun -</option>
                                    <option value="2022">2022</option>
                                    <option value="2021">2021</option>
                                    <option value="2020">2020</option>
                                    <option value="2019">2019</option>
                                    <option value="2018">2018</option>
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2011">2011</option>
                                </select>
                                <button id="btnSubmit" onclick="addInvoice()">Tambah</button>
                            </div>
                            
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width: 30px;">No</th>
                                        <th>Nomor Tagihan</th>
                                        <th>Periode</th>
                                        <th>Nominal</th>
                                        <th style="width: 50px;">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyWrap">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 30px;">No</th>
                                        <th>Nomor Tagihan</th>
                                        <th>Periode</th>
                                        <th>Nominal</th>
                                        <th style="width: 50px;">Opsi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Table End -->


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-light rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Kelompok 2 TB1 Pemrograman Web</a>
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            Universitas Mercubuana
                        </br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/chart/chart.min.js') }}"></script>
    <script src="{{ asset('assets/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('assets/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/lib/tempusdominus/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/lib/tempusdominus/js/moment-timezone.min.js') }}"></script>
    <script src="{{ asset('assets/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script src="{{ asset('assets/js/helper.js') }}"></script>
    <script>
        
        axios.get('/api/unit/').then((response) => {   
            var data = response.data.data;
            
            var fSelectUnit = document.getElementById('fSelectUnit');

            for(var i = 0;i < data.length;i++) {
                var option = fSelectUnit.appendChild(document.createElement('option'));
                option.setAttribute('value', data[i]['id']);
                option.innerHTML = data[i]['blok']['name']+" - "+data[i]['name'];
            }
        });

        function checkAndRemove() {
            var tBodyWrap = document.getElementById('tbodyWrap');
            tBodyWrap.innerHTML = "";
        }
        function fetchData() {
            checkAndRemove();
            
            unitId = document.getElementById('fSelectUnit').value;
            axios.get('/api/unit/'+unitId+'/invoice').then((response) => {  
                console.log(unitId);
                console.log(response); 
                var data = response.data.data;
                var tbodyWrap = document.getElementById('tbodyWrap');
                
                var no = 0;
                    for(var i = 0;i < data.length;i++) {
                        no += 1;
                        var tr = tbodyWrap.appendChild(document.createElement('tr'));
                        tr.classList += "tr-invoice";
                        tr.style.background = data[i]['payment'] == null ? "#ffa1a1" : "#abeeff";
                            
                            var tdNo = tr.appendChild(document.createElement('td'));
                            tdNo.innerHTML = no;
                            
                            var tdNumber = tr.appendChild(document.createElement('td'));
                            tdNumber.innerHTML = data[i]['number'];
                            
                            var tdPeriod = tr.appendChild(document.createElement('td'));
                            tdPeriod.innerHTML = dateFormatInvoice(data[i]['period']);
                            
                            var tdAmount = tr.appendChild(document.createElement('td'));
                            tdAmount.innerHTML = formatRupiah(data[i]['amount']);

                            var tdPay = tr.appendChild(document.createElement('td'));
                            tdPay.style.textAlign = "center";
                                var btnPay = tdPay.appendChild(document.createElement('button'));
                                btnPay.style.fontSize = "10px";
                                if(data[i]['payment'] == null) {
                                    btnPay.innerHTML = "Bayar";
                                    btnPay.setAttribute('onclick', 'payment('+data[i]['id']+')');
                                    btnPay.setAttribute('id', 'pay-'+data[i]['id']);
                                } else {
                                    btnPay.innerHTML = "Lunas <br>"+dateFormat(data[i]['payment']['payment_at']);
                                    btnPay.style.width = "150px";
                                }
                    }
                    
                $('#example').DataTable();
            });
        }

        function addInvoice() {
            
            var unitId = document.getElementById('fSelectUnit').value;
            var fSelectMonth = document.getElementById('fSelectMonth').value;
            var fSelectYear = document.getElementById('fSelectYear').value;

            if(unitId && fSelectMonth && fSelectYear) {
                var dataForm = {
                    "unit_id" : unitId,
                    "period_year" : fSelectYear,
                    "period_month" : fSelectMonth,
                };

                btnSubmit.style.display = "none";

                axios.post('/api/invoice', dataForm).then((response) => {   
                    btnSubmit.style.display = "inline";
                    document.getElementById('fSelectMonth').value = "";
                    document.getElementById('fSelectYear').value = "";
                        
                    fetchData();
                });
            }
        }

        function payment(invoiceId) {
            var dataForm = {
                    "invoice_id" : invoiceId
            };

            var btnSubmit = document.getElementById('pay-'+invoiceId);

            btnSubmit.style.display = "none";

            axios.post('/api/payment', dataForm).then((response) => {   
                btnSubmit.style.display = "inline";
                    
                fetchData();
            });
        }
    </script>

    <!-- Template Javascript -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
</body>

</html>