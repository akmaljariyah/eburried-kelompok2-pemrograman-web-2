<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Kelompok 2 - Pemrograman Web</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600;700&display=swap" rel="stylesheet">
    
    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Libraries Stylesheet -->
    <link href="{{ asset('assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css') }}" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
</head>
<style>
    .m-0 {
        margin: 0px !important;
    }
</style>
<body>
    <div class="container-xxl position-relative bg-white d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->
        <!-- <div class="sidebar pe-4 pb-3">
            <nav class="navbar bg-light navbar-light">
                <a href="/" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary"><i class="fa fa-hashtag me-2"></i>eBurried</h3>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4">
                    <div class="position-relative">
                        <img class="rounded-circle" src="{{ asset('assets/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0">Kelompok 2</h6>
                        <span>Admin</span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="/blok" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Data Blok</a>
                    <a href="/unit" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Data Unit</a>
                    <a href="/invoice" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Data Invoice</a>
                </div>
            </nav>
        </div> -->
        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0" style="display:none">
                <a href="/" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><i class="fa fa-hashtag"></i></h2>
                </a>
                <a id="btnAuto" href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>    
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="{{ asset('assets/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex">Kelompok 2</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                            <a href="#" class="dropdown-item">My Profile</a>
                            <a href="#" class="dropdown-item">Settings</a>
                            <a href="#" class="dropdown-item">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->


            <!-- Table Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-sm-12 col-xl-12">
                        <div class="bg-light rounded h-100 p-4">
                            <h6 class="mb-4">Data Unit</h6>
                            <div style="margin-bottom:20px;">
                                <!-- <select id="fBlok">
                                    <option value="" selected hidden>- Pilih Blok -</option>
                                </select>
                                <input type="text" id="fName" placeholder="Masukan nama blok..."> 
                                <input type="number" id="fCost" placeholder="Masukan biaya iuran..."> 
                                <button id="btnSubmit" onclick="tambahBlok()">Tambah</button> -->
                            </div>
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width: 30px;">No</th>
                                        <th>Nama Unit</th>
                                        <th>Biaya Iuran</th>
                                        <th>Jenazah</th>
                                        <th>Alamat</th>
                                        <th style="width: 50px;">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyWrap">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Unit</th>
                                        <th>Biaya Iuran</th>
                                        <th>Jenazah</th>
                                        <th>Alamat</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Table End -->


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-light rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Kelompok 2 TB1 Pemrograman Web</a>
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            Universitas Mercubuana
                        </br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->

        <!-- Modal -->
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <h4>Unit <span class="unit-name-title"></span> | Tambah Jenazah</h4>
                <br>
                <input id="fNameAddCorpse" type="text" placeholder="Nama Jenazah">
                <br>
                <textarea id="fAddressAddCorpse" placeholder="Alamat Jenazah"></textarea>
                <br>
                <span style="color:black">Tanggal Meninggal</span>
                <input id="fDieAtCorpse" type="date" placeholder="Tanggal Meninggal">
                <br>
                <span style="color:black">Tanggal Dikubur</span>
                <input id="fBurriedAtCorpse" type="date" placeholder="Tanggal Dikubur">
                <br>
                <button id="btnSubmitAddCorpse" onclick="addCorpse()" style="background-color:blue;color:white;border-radius:10px;">Submit</button>
            </div>
        </div>
        
        <!-- Modal View -->
        <!-- The Modal -->
        <div id="modalView" class="modal">
            <!-- Modal content -->
            <div class="modal-content" style="margin: 0px 30px !important;width:95%;">
                <span class="close">&times;</span>
                <h4>Unit <span class="unit-name-title"></span> | Detail Jenazah</h4>
                
                <p class="m-0"><b>Nama : </b> <span id="viewName">Loading...</span></p>
                <p class="m-0"><b>Alamat : </b> <span id="viewAddress">Loading...</span></p>
                <p class="m-0"><b>Tanggal Meninggal : </b> <span id="viewDieAt">Loading...</span></p>
                <p class="m-0"><b>Tanggal Dikubur : </b> <span id="viewBurriedAt">Loading...</span></p>

                <table style="margin-top:20px;margin-bottom:20px;" class="display">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="tBodyViewWrap">
                        <!-- <tr>
                            <td>1</td>
                            <td>Farhan</td>
                            <td>Jl 123</td>
                            <td>08123456</td>
                            <td>farhan@a213.com</td>
                            <td>Anak</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><input type="text" placeholder="Masukan nama..."></td>
                            <td><input type="text" placeholder="Masukan alamat..."></td>
                            <td><input type="text" placeholder="Masukan telepon..."></td>
                            <td><input type="text" placeholder="Masukan email..."></td>
                            <td><input style="width:100%;" type="text" placeholder="Masukan status..."></td>
                        </tr> -->
                    </tbody>
                </table>
                <!-- <button id="buttonSubmitFamily" onclick="addFamilyCorpse()">Add Family</button>      -->
            </div>
        </div>


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/chart/chart.min.js') }}"></script>
    <script src="{{ asset('assets/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('assets/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/lib/tempusdominus/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/lib/tempusdominus/js/moment-timezone.min.js') }}"></script>
    <script src="{{ asset('assets/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script src="{{ asset('assets/js/helper.js') }}"></script>
    <script>          
        
        axios.get('/api/blok').then((response) => {   
                var data = response.data.data;
                var fBlok = document.getElementById('fBlok');

                for(var i = 0;i < data.length;i++) {
                    var option = fBlok.appendChild(document.createElement('option'));
                    option.setAttribute('value', data[i]['id']);
                    option.innerHTML = data[i]['name'];
                }
            });

        function checkAndRemove() {
            var trUnits = document.getElementsByClassName('tr-unit');

            if(trUnits.length > 0) {
                for(var i = (trUnits.length - 1);i >= 0;i--) {
                    trUnits[i].remove();
                }
            } 
        }
        
        function setModal() {
            // Get the modal
            var modal = document.getElementById("myModal");
            var modalView = document.getElementById("modalView");

            // Get the button that opens the modal
            var btnEdits = document.getElementsByClassName("btn-edit-modal");
            var btnViewModals = document.getElementsByClassName("btn-view-modal");

            // Get the <span> element that closes the modal
            var span1 = document.getElementsByClassName("close")[0];
            var span2 = document.getElementsByClassName("close")[1];

            // When the user clicks the button, open the modal 
            for(var i = 0;i < btnEdits.length;i++) {
                btnEdits[i].onclick = function() {
                    modal.style.display = "block";
                    console.log('a');
                }
            }

            for(var i = 0;i < btnViewModals.length;i++) {
                btnViewModals[i].onclick = function() {
                    modalView.style.display = "block";
                    console.log('b');
                }
            }

            // When the user clicks on <span> (x), close the modal
            span1.onclick = function() {
                modal.style.display = "none";
            }
            span2.onclick = function() {
                modalView.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
                
                if (event.target == modalView) {
                    modalView.style.display = "none";
                }
            }
        }

        function fetchData() {
            checkAndRemove();

            axios.get('/api/unit').then((response) => {   
                var data = response.data.data;
                var tbodyWrap = document.getElementById('tbodyWrap');
                
                var no = 0;
                    for(var i = 0;i < data.length;i++) {
                        no += 1;
                        var tr = tbodyWrap.appendChild(document.createElement('tr'));
                        tr.classList += "tr-unit";
                        tr.style.background = data[i]['status'] == "available" ? "#abeeff" : "#ffa1a1";
                        tr.style.color = "black";
                            
                            var tdNo = tr.appendChild(document.createElement('td'));
                            tdNo.innerHTML = no;
                            
                            var tdName = tr.appendChild(document.createElement('td'));
                            tdName.setAttribute('id', 'name-'+data[i]['id']);
                            tdName.innerHTML = data[i]['blok']['name'] + " - " +data[i]['name'];
                            
                            var tdIuran = tr.appendChild(document.createElement('td'));
                            tdIuran.innerHTML = formatRupiah(data[i]['cost_per_month'], 'Rp. ');
                            
                            var tdCorpse = tr.appendChild(document.createElement('td'));
                            tdCorpse.innerHTML = data[i]['corpse'] == null ? '' : data[i]['corpse']['name'];
                            
                            var tdCorpseAddress = tr.appendChild(document.createElement('td'));
                            tdCorpseAddress.innerHTML = data[i]['corpse'] == null ? '' : data[i]['corpse']['address'];
                            
                            var tdEdit = tr.appendChild(document.createElement('td'));
                            if(data[i]['status'] != "available") {
                                var btnEdit = tdEdit.appendChild(document.createElement('button'));

                                btnEdit.innerHTML = data[i]['status'] == "available" ? "Add" : "View";

                                if(data[i]['status'] == "available") {
                                    btnEdit.classList += "btn-edit-modal";
                                    tdEdit.setAttribute('onclick', 'setUnitId('+data[i]['id']+')');
                                } else {
                                    btnEdit.classList += "btn-view-modal";
                                    tdEdit.setAttribute('onclick', 'setUnitId('+data[i]['id']+'), setCorpseId('+data[i]['corpse']['id']+'), showCorpse('+data[i]['corpse']['id']+')');
                                }
                            }
                    }
                    
                $('#example').DataTable();
                setModal();
            });
        }

        fetchData();

        window.unitId = 0;
        function setUnitId(id) {
            window.unitId = id;
            unitNameTitles = document.getElementsByClassName('unit-name-title');
            for(var i = 0;i < unitNameTitles.length;i++) {
                unitNameTitles[i].innerHTML = document.getElementById('name-'+window.unitId).textContent;
            }
        }
        
        window.corpseId = 0;
        function setCorpseId(id) {
            window.corpseId = id;
        }

        function addFamilyCorpse() {

            var fNameAddFamily = document.getElementById('fNameAddFamily').value;
            var fAddressAddFamily = document.getElementById('fAddressAddFamily').value;
            var fPhoneAddFamily = document.getElementById('fPhoneAddFamily').value;
            var fEmailAddFamily = document.getElementById('fEmailAddFamily').value;
            var fStatusAddFamily = document.getElementById('fStatusAddFamily').value;
            var buttonSubmitFamily = document.getElementById('buttonSubmitFamily');

            if(fNameAddFamily && fAddressAddFamily && fPhoneAddFamily && fEmailAddFamily && fStatusAddFamily) {
                var dataForm = {
                    "corpse_id" : window.corpseId,
                    "name" : fNameAddFamily,
                    "address" : fAddressAddFamily,
                    "phone" : fPhoneAddFamily,
                    "email" : fEmailAddFamily,
                    "status" : fStatusAddFamily,
                };

                buttonSubmitFamily.style.display = "none";

                axios.post('/api/family', dataForm).then((response) => {   
                    buttonSubmitFamily.style.display = "inline";                        
                    showCorpse(window.corpseId);
                });
            }
        }

        function showCorpse(corpseId) {           
            document.getElementById('tBodyViewWrap').innerHTML = "";
            axios.get('/api/corpse/'+corpseId).then((response) => {
                var data = response.data.data;
                var dataFamilies = data['families'];

                var viewName = document.getElementById('viewName');
                var viewAddress = document.getElementById('viewAddress');
                var viewDieAt = document.getElementById('viewDieAt');
                var viewBurriedAt = document.getElementById('viewBurriedAt');

                viewName.innerHTML = data['name'];
                viewAddress.innerHTML = data['address'];
                viewDieAt.innerHTML = dateFormat(data['die_at']);
                viewBurriedAt.innerHTML = dateFormat(data['burried_at']);

                var tBodyViewWrap = document.getElementById('tBodyViewWrap');
                var number = 0;

                    for(var i = 0;i < dataFamilies.length;i++) {
                        var tr = tBodyViewWrap.appendChild(document.createElement('tr'));

                            var tdNo = tr.appendChild(document.createElement('td'));
                            number += 1;
                            tdNo.innerHTML = number;
                            
                            var tdName = tr.appendChild(document.createElement('td'));
                            tdName.innerHTML = dataFamilies[i]['name'];
                            
                            var tdAddress = tr.appendChild(document.createElement('td'));
                            tdAddress.innerHTML = dataFamilies[i]['address'];
                            
                            var tdPhone = tr.appendChild(document.createElement('td'));
                            tdPhone.innerHTML = dataFamilies[i]['phone'];
                            
                            var tdEmail = tr.appendChild(document.createElement('td'));
                            tdEmail.innerHTML = dataFamilies[i]['email'];
                            
                            var tdStatus = tr.appendChild(document.createElement('td'));
                            tdStatus.innerHTML = dataFamilies[i]['status'];        
                    }

                    var trInput = tBodyViewWrap.appendChild(document.createElement('tr'));
                    // trInput.innerHTML = "<td>"+(number+1)+"</td><td><input type='text' placeholder='Masukan nama...' id='fNameAddFamily'></td><td><input type='text' placeholder='Masukan alamat...' id='fAddressAddFamily'></td><td><input type='text' placeholder='Masukan telepon...' id='fPhoneAddFamily'></td><td><input type='text' placeholder='Masukan email...' id='fEmailAddFamily'></td><td><input style='width:100%;' type='text' placeholder='Masukan status...' id='fStatusAddFamily'></td>";
            });   
        }

        function tambahBlok() {
            var fBlok = document.getElementById('fBlok').value;
            var fName = document.getElementById('fName').value;
            var fCost = document.getElementById('fCost').value;
            var btnSubmit = document.getElementById('btnSubmit');

            if(fName && fBlok && fCost) {
                var dataForm = {
                    "blok_id" : fBlok,
                    "name" : fName,
                    "cost_per_month" : fCost
                };

                btnSubmit.style.display = "none";

                axios.post('/api/unit', dataForm).then((response) => {   
                    btnSubmit.style.display = "inline";
                    document.getElementById('fName').value = "";
                    document.getElementById('fCost').value = "";
                        
                    fetchData();
                });
            }
        }

        function addCorpse() {
            
            var fNameAddCorpse = document.getElementById('fNameAddCorpse').value;
            var fAddressAddCorpse = document.getElementById('fAddressAddCorpse').value;
            var fDieAtCorpse = document.getElementById('fDieAtCorpse').value;
            var fBurriedAtCorpse = document.getElementById('fBurriedAtCorpse').value;
            var btnSubmitAddCorpse = document.getElementById('btnSubmitAddCorpse');

            if(fNameAddCorpse && fAddressAddCorpse && fDieAtCorpse && fBurriedAtCorpse) {
                var dataForm = {
                    "unit_id" : window.unitId,
                    "name" : fNameAddCorpse,
                    "address" : fAddressAddCorpse,
                    "die_at" : fDieAtCorpse,
                    "burried_at" : fBurriedAtCorpse
                };

                btnSubmitAddCorpse.style.display = "none";
                console.log(dataForm);
                axios.post('/api/corpse', dataForm).then((response) => {   
                    btnSubmitAddCorpse.style.display = "inline";
                    document.getElementById('fNameAddCorpse').value = "";
                    document.getElementById('fAddressAddCorpse').value = "";
                    document.getElementById('fDieAtCorpse').value = "";
                    document.getElementById('fBurriedAtCorpse').value = "";
                    
                    var spanClose = document.getElementsByClassName("close")[0];
                    spanClose.click();


                    fetchData();
                });
            }
        }

        
    </script>

    <!-- Template Javascript -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script>
        document.getElementById('btnAuto').click();  
    </script>
</body>

</html>